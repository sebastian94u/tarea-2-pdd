#include "../include/EspecialistaPanDulce.h"

#include <iostream>

EspecialistaPanDulce::EspecialistaPanDulce(const std::string& nombre, int edad)
    : Empleado(nombre, edad) {}

void EspecialistaPanDulce::Trabajar() const {
    std::cout << "Elaborando pan dulce." << std::endl;
}
