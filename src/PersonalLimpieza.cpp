#include "../include/PersonalLimpieza.h"

#include <iostream>

PersonalLimpieza::PersonalLimpieza(const std::string& nombre, int edad)
    : Empleado(nombre, edad) {}

void PersonalLimpieza::Trabajar() const {
    std::cout << "Realizando tareas de limpieza." << std::endl;
}
