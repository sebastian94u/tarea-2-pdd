#include "../include/Turno.h"
#include "../include/TipoDePan.h"

#include <iostream>
#include <string>
#include <vector>

Turno::Turno(const std::string& nombre) : nombre_turno(nombre) {}

Turno::~Turno() {}

std::string Turno::GetNombreTurno() const {
    return nombre_turno;
}

void Turno::ProductosTurno() const {}

void Turno::AgregarTipoDePan() {}

void Turno::VerTiposDePan() const {}

void Turno::AgregarEmpleado() {}

void Turno::VerEmpleados() const {}
