#include "../include/TurnoT.h"

std::string TurnoT::GetNombreTurno() const {
  return "Turno Mañana";
}

void TurnoT::ProductosTurno() const {
  std::cout << "Productos del turno mañana" << std::endl;
}
