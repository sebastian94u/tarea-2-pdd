#include "../include/PanDulce.h"

#include <iostream>
#include <string>

PanDulce::PanDulce(const std::string& descripcion, double cantidad_harina, double cantidad_azucar,
                   double cantidad_agua, double cantidad_levadura, double cantidad_sal,
                   const std::string& relleno, const std::string& sabor)
    : TipoDePan(descripcion, cantidad_harina, cantidad_azucar, cantidad_agua, cantidad_levadura, cantidad_sal),
      relleno(relleno),
      sabor(sabor) {}

std::string PanDulce::GetRelleno() const {
    return relleno;
}

void PanDulce::SetRelleno(const std::string& relleno) {
    this->relleno = relleno;
}

std::string PanDulce::GetSabor() const {
    return sabor;
}

void PanDulce::SetSabor(const std::string& sabor) {
    this->sabor = sabor;
}

void PanDulce::MostrarInformacion() const {
    std::cout << "Pan Dulce - Descripción: " << GetDescripcion() << ", Relleno: " << relleno << ", Sabor: " << sabor << std::endl;
}
