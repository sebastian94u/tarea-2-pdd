#include "../include/PanSalado.h"

#include <iostream>
#include <string>

PanSalado::PanSalado(const std::string& descripcion, double cantidad_harina, double cantidad_azucar,
                   double cantidad_agua, double cantidad_levadura, double cantidad_sal,
                   const std::string& relleno, const std::string& sabor)
    : TipoDePan(descripcion, cantidad_harina, cantidad_azucar, cantidad_agua, cantidad_levadura, cantidad_sal),
      relleno(relleno),
      sabor(sabor) {}

std::string PanSalado::GetRelleno() const {
    return relleno;
}

void PanSalado::SetRelleno(const std::string& relleno) {
    this->relleno = relleno;
}

std::string PanSalado::GetSabor() const {
    return sabor;
}

void PanSalado::SetSabor(const std::string& sabor) {
    this->sabor = sabor;
}

void PanSalado::MostrarInformacion() const {
    std::cout << "Pan Salado - Descripción: " << GetDescripcion() << ", Relleno: " << relleno << ", Sabor: " << sabor << std::endl;
}
