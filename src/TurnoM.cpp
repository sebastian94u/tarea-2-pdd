#include "../include/TurnoM.h"

std::string TurnoM::GetNombreTurno() const {
  return "Turno Mañana";
}

void TurnoM::ProductosTurno() const {
  std::cout << "Productos del turno mañana" << std::endl;
}
