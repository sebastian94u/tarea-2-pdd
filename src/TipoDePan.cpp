#include "../include/TipoDePan.h"

TipoDePan::TipoDePan(const std::string& descripcion, double cantidad_harina, double cantidad_azucar,
                     double cantidad_agua, double cantidad_levadura, double cantidad_sal)
    : descripcion(descripcion), cantidad_harina(cantidad_harina), cantidad_azucar(cantidad_azucar),
      cantidad_agua(cantidad_agua), cantidad_levadura(cantidad_levadura), cantidad_sal(cantidad_sal) {}

std::string TipoDePan::GetDescripcion() const {
    return descripcion;
}

void TipoDePan::SetDescripcion(const std::string& descripcion) {
    this->descripcion = descripcion;
}

double TipoDePan::GetCantidadHarina() const {
    return cantidad_harina;
}

void TipoDePan::SetCantidadHarina(double cantidad) {
    cantidad_harina = cantidad;
}

double TipoDePan::GetCantidadAzucar() const {
    return cantidad_azucar;
}

void TipoDePan::SetCantidadAzucar(double cantidad) {
    cantidad_azucar = cantidad;
}

double TipoDePan::GetCantidadAgua() const {
    return cantidad_agua;
}

void TipoDePan::SetCantidadAgua(double cantidad) {
    cantidad_agua = cantidad;
}

double TipoDePan::GetCantidadLevadura() const {
    return cantidad_levadura;
}

void TipoDePan::SetCantidadLevadura(double cantidad) {
    cantidad_levadura = cantidad;
}

double TipoDePan::GetCantidadSal() const {
    return cantidad_sal;
}

void TipoDePan::SetCantidadSal(double cantidad) {
    cantidad_sal = cantidad;
}
