#include "../include/EspecialistaGalleta.h"

#include <iostream>

EspecialistaGalleta::EspecialistaGalleta(const std::string& nombre, int edad)
    : Empleado(nombre, edad) {}

void EspecialistaGalleta::Trabajar() const {
    std::cout << "Elaborando galletas." << std::endl;
}
