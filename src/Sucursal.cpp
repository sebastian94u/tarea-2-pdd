#include "../include/Sucursal.h"

#include <iostream>

std::string Sucursal::GetUbicacion() const {
    return ubicacion;
}

void Sucursal::SetUbicacion(std::string value) {
    ubicacion = value;
}

int Sucursal::GetNumeroSucursal() const {
    return numero_sucursal;
}

void Sucursal::SetNumeroSucursal(int value) {
    numero_sucursal = value;
}

std::string Sucursal::GetNumeroTelefono() const {
    return numero_telefono;
}

void Sucursal::SetNumeroTelefono(std::string value) {
    numero_telefono = value;
}

void Sucursal::AgregarTurno() {
    // Implementación para agregar un nuevo turno a la sucursal
}

void Sucursal::VerInformacionSucursal() const {
    // Implementación para mostrar la información de la sucursal
}
