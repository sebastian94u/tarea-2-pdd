#include "../include/EspecialistaPanSalado.h"

#include <iostream>

EspecialistaPanSalado::EspecialistaPanSalado(const std::string& nombre, int edad)
    : Empleado(nombre, edad) {}

void EspecialistaPanSalado::Trabajar() const {
    std::cout << "Elaborando pan salado." << std::endl;
}
