#include "../include/EncargadoPersonal.h"

#include <iostream>

EncargadoPersonal::EncargadoPersonal(const std::string& nombre, int edad)
    : Empleado(nombre, edad) {}

void EncargadoPersonal::Trabajar() const {
    std::cout << "Supervisando el personal y coordinando las tareas." << std::endl;
}
