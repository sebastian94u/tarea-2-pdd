#include "../include/Gerente.h"

#include <iostream>

Gerente::Gerente(const std::string& nombre, int edad)
    : Empleado(nombre, edad) {}

void Gerente::Trabajar() const {
    std::cout << "Gestionando sucursal." << std::endl;
}
