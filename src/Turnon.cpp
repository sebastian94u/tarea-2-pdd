#include "../include/TurnoN.h"

std::string TurnoN::GetNombreTurno() const {
  return "Turno Mañana";
}

void TurnoN::ProductosTurno() const {
  std::cout << "Productos del turno mañana" << std::endl;
}
