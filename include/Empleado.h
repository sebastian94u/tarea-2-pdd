#ifndef EMPLEADO_H
#define EMPLEADO_H

#include <string>

class Empleado {
private:
    std::string nombre;
    int edad;

public:
    Empleado(const std::string& nombre, int edad);

    std::string GetNombre() const;
    void SetNombre(const std::string& nombre);

    int GetEdad() const;
    void SetEdad(int edad);

    virtual void Trabajar() const = 0;
};

#endif
