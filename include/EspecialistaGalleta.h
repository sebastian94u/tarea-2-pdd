#ifndef ESPECIALISTAGALLETA_H
#define ESPECIALISTAGALLETA_H

#include "Empleado.h"

class EspecialistaGalleta : public Empleado {
public:
    EspecialistaGalleta(const std::string& nombre, int edad);

    void Trabajar() const override;
};

#endif
