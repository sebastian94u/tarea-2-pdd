#ifndef PERSONALLIMPIEZA_H
#define PERSONALLIMPIEZA_H

#include "Empleado.h"

class PersonalLimpieza : public Empleado {
public:
    PersonalLimpieza(const std::string& nombre, int edad);

    void Trabajar() const override;
};

#endif
