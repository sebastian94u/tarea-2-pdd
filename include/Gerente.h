#ifndef GERENTE_H
#define GERENTE_H

#include "Empleado.h"

class Gerente : public Empleado {
public:
    Gerente(const std::string& nombre, int edad);

    void Trabajar() const override;
};

#endif
