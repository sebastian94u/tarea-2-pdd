#ifndef SUCURSAL_H
#define SUCURSAL_H

#include "Turno.h"

#include <iostream>
#include <string>
#include <vector>

class Sucursal {
  private:
    std::string ubicacion;
    int numero_sucursal;
    std::string numero_telefono;
    // OBJETO PARA GERENTE DE TIPO EMPLEADO - POR DEFINIR!
    std::vector<Turno> turnos;
  public:
    std::string GetUbicacion() const;
    void SetUbicacion(std::string value);

    int GetNumeroSucursal() const;
    void SetNumeroSucursal(int value);

    std::string GetNumeroTelefono() const;
    void SetNumeroTelefono(std::string value);

    void AgregarTurno();
    void VerInformacionSucursal() const;
};

#endif
