#ifndef TURNOT_H
#define TURNOT_H

#include "Turno.h"

#include <iostream>
#include <string>
#include <vector>

class TurnoT : public Turno {
  private:
  public:
    std::string GetNombreTurno() const override;
    void ProductosTurno() const override;
};

#endif
