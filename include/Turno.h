#ifndef TURNO_H
#define TURNO_H

#include "TipoDePan.h"

#include <iostream>
#include <string>
#include <vector>

class Turno {
  private:
    std::string nombre_turno;
    std::vector<TipoDePan> tipos_de_pan;
    // std::vector<Empleado> empleados;
  public:
    Turno(const std::string& nombre);
    virtual ~Turno();
    virtual std::string GetNombreTurno() const = 0;
    virtual void ProductosTurno() const = 0;
    virtual void AgregarTipoDePan();
    virtual void VerTiposDePan() const;
    virtual void AgregarEmpleado();
    virtual void VerEmpleados() const;
};

#endif
