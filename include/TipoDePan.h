#ifndef TIPODEPAN_H
#define TIPODEPAN_H

#include <string>

class TipoDePan {
private:
    std::string descripcion;
    double cantidad_harina;
    double cantidad_azucar;
    double cantidad_agua;
    double cantidad_levadura;
    double cantidad_sal;

public:

  TipoDePan(const std::string& descripcion, double cantidad_harina, double cantidad_azucar,
              double cantidad_agua, double cantidad_levadura, double cantidad_sal);

    std::string GetDescripcion() const;
    void SetDescripcion(const std::string& descripcion);

    double GetCantidadHarina() const;
    void SetCantidadHarina(double cantidad);

    double GetCantidadAzucar() const;
    void SetCantidadAzucar(double cantidad);

    double GetCantidadAgua() const;
    void SetCantidadAgua(double cantidad);

    double GetCantidadLevadura() const;
    void SetCantidadLevadura(double cantidad);

    double GetCantidadSal() const;
    void SetCantidadSal(double cantidad);
};

#endif
