#ifndef PANADERIA_H
#define PANADERIA_H

#include "Sucursal.h"

#include <iostream>
#include <vector>

class Panaderia {
  private:
    std::vector<Sucursal> sucursales;
  public:
    Panaderia();
    void AgregarSucursal();
    void VerSucursales();
};

#endif
