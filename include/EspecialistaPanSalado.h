#ifndef ESPECIALISTAPANSALADO_H
#define ESPECIALISTAPANSALADO_H

#include "Empleado.h"

class EspecialistaPanSalado : public Empleado {
public:
    EspecialistaPanSalado(const std::string& nombre, int edad);

    void Trabajar() const override;
};

#endif
