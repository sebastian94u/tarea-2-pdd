#ifndef ENCARGADOPERSONAL_H
#define ENCARGADOPERSONAL_H

#include "Empleado.h"

class EncargadoPersonal : public Empleado {
public:
    EncargadoPersonal(const std::string& nombre, int edad);

    void Trabajar() const override;
};

#endif
