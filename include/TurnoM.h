#ifndef TURNOM_H
#define TURNOM_H

#include "Turno.h"

#include <iostream>
#include <string>
#include <vector>

class TurnoM : public Turno {
  private:
  public:
    std::string GetNombreTurno() const override;
    void ProductosTurno() const override;
};

#endif
