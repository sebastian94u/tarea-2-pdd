#ifndef PANDULCE_H
#define PANDULCE_H

#include "TipoDePan.h"

#include <string>

class PanDulce : public TipoDePan
{
private:
  std::string relleno;
  std::string sabor;

public:
  PanDulce(const std::string &descripcion, double cantidad_harina, double cantidad_azucar,
                     double cantidad_agua, double cantidad_levadura, double cantidad_sal,
                     const std::string &relleno, const std::string &sabor);

  std::string GetRelleno() const;

  void SetRelleno(const std::string &relleno);

  std::string GetSabor() const;

  void SetSabor(const std::string &sabor);

  void MostrarInformacion() const;
};
#endif
