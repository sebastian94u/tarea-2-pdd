#ifndef ESPECIALISTAPANDULCE_H
#define ESPECIALISTAPANDULCE_H

#include "Empleado.h"

class EspecialistaPanDulce : public Empleado {
public:
    EspecialistaPanDulce(const std::string& nombre, int edad);

    void Trabajar() const override;
};

#endif
