#ifndef TURNON_H
#define TURNON_H

#include "Turno.h"

#include <iostream>
#include <string>
#include <vector>

class TurnoN : public Turno {
  private:
  public:
    std::string GetNombreTurno() const override;
    void ProductosTurno() const override;
};

#endif
